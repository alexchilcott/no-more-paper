import { ApiHost, IApiHost, IApiHostConfiguration } from './apihost';
import { GetDocumentListHandler } from './bounded-contexts/documents/api-handlers/GetDocumentListHandler';
import { GetSingleDocumentHandler } from './bounded-contexts/documents/api-handlers/GetSingleDocumentHandler';
import { SetDocumentMetadataHandler } from './bounded-contexts/documents/api-handlers/SetDocumentMetadataHandler';
import { UploadDocumentHandler } from './bounded-contexts/documents/api-handlers/UploadDocumentHandler';
import { DocumentListReadModelEventConsumer } from './bounded-contexts/documents/consumers/DocumentListReadModelEventConsumer';
import { ThumbnailGeneratorConsumer } from './bounded-contexts/documents/consumers/ThumbnailGeneratorConsumer';
import { DocumentReadModel } from './bounded-contexts/documents/read-models/DocumentsReadModel';
import { ThumbnailGenerator } from './bounded-contexts/documents/services/ThumbnailGenerator';
import { GetFileByIdHandler } from './bounded-contexts/files/api-handlers/GetFileByIdHandler';
import { FileClient } from './bounded-contexts/files/client/FileClient';
import { IDocumentStorageConfiguration } from './bounded-contexts/files/configuration/DocumentStorageConfiguration';
import { FileRepository } from './bounded-contexts/files/repositories/FileBinaryRepository';
import { DocumentSearchApiHandler } from './bounded-contexts/search/api-handlers/DocumentSearchApiHandler';
import { TextIndexingDocumentConsumer } from './bounded-contexts/search/consumers/TextIndexingDocumentConsumer';
import { TextIndexResultsReadModel } from './bounded-contexts/search/read-models/TextIndexResultsReadModel';
import { PdfTextIndexerStrategy } from './bounded-contexts/search/services/PdfTextIndexerStrategy';
import { TextIndexer } from './bounded-contexts/search/services/TextIndexer';
import { Clock } from './frameworks/clock/Clock';
import { ConsumerManager, IConsumerManager } from './frameworks/consumers/ConsumerManager';
import { TempDirectoryProvider } from './frameworks/fs/TempDirectoryProvider';
import { arrayOfAll, constructor, container, resolve, singleton, value } from './frameworks/ioc/Container';
import { IKafkaClientConfiguration, KafkaClientProvider } from './frameworks/kafka/KafkaClientProvider';
import { KafkaConsumer } from './frameworks/kafka/KafkaConsumer';
import { KafkaEventConsumer } from './frameworks/kafka/KafkaEventConsumer';
import { KafkaEventPublisher } from './frameworks/kafka/KafkaEventPublisher';
import { KafkaProducer } from './frameworks/kafka/KafkaProducer';
import { IPostgresClientProviderConfiguration, PostgresClientProvider } from './frameworks/postgres/PostgresClientProvider';
import { IRunnable } from './frameworks/runnables/IRunnable';
import { RunnableAdapter } from './frameworks/runnables/RunnableAdapter';

interface IConfig {
  documentStorage: IDocumentStorageConfiguration;
  apiHost: IApiHostConfiguration;
  kafka: IKafkaClientConfiguration;
  postgres: IPostgresClientProviderConfiguration;
}

const config: IConfig = {
  documentStorage: {
    documentStoragePath: '/data/file-storage',
  },
  apiHost: {
    port: 80,
    basePath: '/api',
  },
  kafka: {
    zookeeperHost: 'zookeeper',
    port: 2181,
    clientId: 'no-more-paper',
  },
  postgres: {
    user: 'postgres',
    password: 'postgres',
    database: 'no_more_paper',
    port: 5432,
    host: 'db',
  },
};

function configureRunnable<T>(key: string, start: (item: T) => Promise<void>, stop: (item: T) => Promise<void>) {
  return singleton(
    resolve(r => {
      const underlying = r.resolve<T>(key);
      return new RunnableAdapter(() => start(underlying), () => stop(underlying));
    }),
  );
}

async function run(): Promise<void> {
  const c = container()
    // kafka
    .configure('kafkaClientConfiguration', value(config.kafka))
    .configure('kafkaClientProvider', singleton(constructor(KafkaClientProvider)))
    .configure('kafkaProducer', singleton(constructor(KafkaProducer)))
    .configure('kafkaConsumer', singleton(constructor(KafkaConsumer)))
    .configure('kafkaEventPublisher', singleton(constructor(KafkaEventPublisher)))
    .configure('kafkaEventConsumer', singleton(constructor(KafkaEventConsumer)))

    // clock
    .configure('clock', singleton(constructor(Clock)))

    // postgres
    .configure('postgresClientProviderConfiguration', value(config.postgres))
    .configure('postgresClientProvider', singleton(constructor(PostgresClientProvider)))

    // fs
    .configure('tempDirectoryProvider', singleton(constructor(TempDirectoryProvider)))

    // files bounded context
    .configure('documentStorageConfiguration', value(config.documentStorage))
    .configure('fileRepository', singleton(constructor(FileRepository)))
    .configure('fileClient', singleton(constructor(FileClient)))
    .configure('apiHandler', singleton(constructor(GetFileByIdHandler)))

    // documents bounded context
    .configure('documentReadModel', singleton(constructor(DocumentReadModel)))
    .configure('thumbnailGenerator', singleton(constructor(ThumbnailGenerator)))
    .configure('apiHandler', singleton(constructor(GetSingleDocumentHandler)))
    .configure('apiHandler', singleton(constructor(GetDocumentListHandler)))
    .configure('apiHandler', singleton(constructor(UploadDocumentHandler)))
    .configure('apiHandler', singleton(constructor(SetDocumentMetadataHandler)))
    .configure('consumer', singleton(constructor(DocumentListReadModelEventConsumer)))
    .configure('consumer', singleton(constructor(ThumbnailGeneratorConsumer)))

    // search bounded context
    .configure('textIndexingStrategy', singleton(constructor(PdfTextIndexerStrategy)))
    .configure('textIndexingStrategies', arrayOfAll('textIndexingStrategy'))
    .configure('textIndexer', singleton(constructor(TextIndexer)))
    .configure('textIndexResultsReadModel', singleton(constructor(TextIndexResultsReadModel)))
    .configure('apiHandler', singleton(constructor(DocumentSearchApiHandler)))
    .configure('consumer', singleton(constructor(TextIndexingDocumentConsumer)))

    // api
    .configure('apiHandlers', arrayOfAll('apiHandler'))
    .configure('apiHostConfiguration', value(config.apiHost))
    .configure('apiHost', singleton(constructor(ApiHost)))
    .configure('runnable', configureRunnable<IApiHost>('apiHost', async host => host.start(), async host => host.stop()))

    // consumers fw
    .configure('consumers', arrayOfAll('consumer'))
    .configure('consumerManager', singleton(constructor(ConsumerManager)))
    .configure('runnable', configureRunnable<IConsumerManager>('consumerManager', m => m.subscribe(), m => m.unsubscribe()));

  const resolver = c.resolver;

  try {
    console.log('Starting...');
    const runnables = resolver.resolveAll<IRunnable>('runnable');

    for (const runnable of runnables) {
      await runnable.start();
    }
    console.log('Started.');
  } catch (err) {
    console.error(err);
  }
}

process.on('unhandledRejection', (reason, promise) => {
  console.log('Unhandled Rejection at:', reason.stack || reason);
  process.exit(-100);
});

run().catch(x => {
  console.error(x);
  process.exit(-101);
});
