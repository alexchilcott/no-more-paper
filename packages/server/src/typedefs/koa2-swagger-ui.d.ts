//import koa = require('koa');

declare module 'koa2-swagger-ui' {
  type koaSwaggerOptions = {
    routePrefix: string;
    swaggerOptions: {
      url: string;
    };
  };

  function koaFunction(opts: koaSwaggerOptions): any;
  export = koaFunction;
}
