declare module 'promisepipe' {
  import { Stream } from 'stream';
  function promisePipeFunc(readableStream: Stream, ...transformStreams: Stream[]): any;
  export = promisePipeFunc;
}
