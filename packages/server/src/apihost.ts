import * as fs from 'fs';
import { Server } from 'http';
import Koa = require('koa');
import koaBody = require('koa-body');
import logger = require('koa-logger');
import Router = require('koa-router');
import serve = require('koa-static');
import koaSwagger = require('koa2-swagger-ui');
import path = require('path');
import { stat } from './frameworks/fs/stat';
import { IApiHandler, isBodyResponse, isHeadersResponse, isSendFileResponse } from './frameworks/koa/IApiHandler';

export interface IApiHost {
  start(): void;
  stop(): void;
}

export interface IApiHostConfiguration {
  port: number;
  basePath: string;
}

export class ApiHost implements IApiHost {
  private server: Server | null = null;

  constructor(private readonly apiHostConfiguration: IApiHostConfiguration, private readonly apiHandlers: IApiHandler[]) {}

  public start() {
    const app = new Koa();
    app.use(logger());
    app.use(koaBody({ multipart: true }));
    app.use(
      koaSwagger({
        routePrefix: '/swagger',
        swaggerOptions: { url: './swagger.yaml' },
      }),
    );

    const apiRouter = new Router();
    for (const handler of this.apiHandlers) {
      const route = handler.route;
      apiRouter.register(route.path, [route.method], (ctx, next) => this.handle(ctx, next, handler));
    }
    app.use(apiRouter.routes());

    const script = process.argv[1];
    const scriptFolder = path.dirname(script);
    const staticFolder = path.join(scriptFolder, 'static');
    app.use(serve(staticFolder));

    this.server = app.listen(this.apiHostConfiguration.port);
  }

  public stop() {
    if (this.server) {
      this.server.close();
    }
  }

  private async handle(context: Koa.Context, next: any, handler: IApiHandler): Promise<void> {
    const response = await handler.handle(context);
    context.response.status = response.statusCode;
    if (isBodyResponse(response)) {
      context.response.body = response.body;
    } else {
      context.response.body = {};
    }
    if (isHeadersResponse(response)) {
      for (const header of Object.keys(response.headers)) {
        context.set(header, response.headers[header]);
      }
    }
    if (isSendFileResponse(response)) {
      const stats = await stat(response.fileToSend);
      context.response.set('Content-Disposition', `attachment; filename="${response.filename}"`);
      context.response.set('Content-Length', `${stats.size}`);
      context.response.body = fs.createReadStream(response.fileToSend);
      context.response.type = path.extname(response.filename);
    }
  }
}
