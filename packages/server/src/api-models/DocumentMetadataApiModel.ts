export interface IDocumentMetadataApiModel {
  title: string;
  createdTimestamp: string;
}
