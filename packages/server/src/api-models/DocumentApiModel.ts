export interface IDocumentApiModel {
  id: string;
  filename: string;
  size: number;
  uploadTimestamp: string;
  createdTimestamp: string;
  fileUrl: string;
  thumbnailUrl: string;
  title: string;
}
