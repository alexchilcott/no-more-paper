export interface IResolver<TKeySet extends string> {
  resolve<T>(key: TKeySet): T;
  resolveAll<T>(key: TKeySet): T[];
}

export interface IDefinition<TKeySet extends string> {
  resolve(resolver: IResolver<TKeySet>): any;
}

export function resolve<TKeySet extends string>(resolveFunc: ((resolver: IResolver<TKeySet>) => any)): IDefinition<TKeySet> {
  return {
    resolve: resolveFunc,
  };
}

export function value<TKeySet extends string>(val: any): IDefinition<TKeySet> {
  return resolve(_ => val);
}

export function alias<TKeySet extends string>(key: TKeySet): IDefinition<TKeySet> {
  return resolve(r => r.resolve(key));
}

export function arrayOfAll<TKeySet extends string>(key: TKeySet): IDefinition<TKeySet> {
  return resolve(r => r.resolveAll(key));
}

export function singleton<TKeySet extends string>(definition: IDefinition<TKeySet>): IDefinition<TKeySet> {
  let v: { resolved: false } | { resolved: true; value: any } = {
    resolved: false,
  };
  return resolve(resolver => {
    if (!v.resolved) {
      const resolvedValue = definition.resolve(resolver);
      v = { resolved: true, value: resolvedValue };
    }
    return v.value;
  });
}

function factoryOrConstructor<TKeySet extends string>(factoryFunction: any, type: 'factory' | 'constructor'): IDefinition<TKeySet> {
  if (typeof factoryFunction !== 'function') {
    throw new Error('value was not a function');
  }

  const FN_ARGS = /^function\s*([^\(]*)\(\s*([^\)]*)\)/m;
  const FN_ARG_SPLIT = /,/;
  const FN_ARG = /^\s*(_?)(.+?)\1\s*$/;
  const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/gm;
  const fnArgs: string[] = [];
  const fnText = factoryFunction.toString().replace(STRIP_COMMENTS, '');
  const argDecl = fnText.match(FN_ARGS);
  argDecl[2].split(FN_ARG_SPLIT).forEach((arg: string) => {
    arg.replace(FN_ARG, (substring, ...args: any[]) => {
      fnArgs.push(args[1]);
      return '';
    });
  });

  // we are forced to lose type safety and we check that the keys are registered at runtime
  const keys = fnArgs as TKeySet[];
  return resolve(resolver => {
    const resolvedDependencies = keys.map(dep => resolver.resolve(dep as TKeySet));
    if (resolvedDependencies.length === 0) {
      if (type === 'factory') {
        return factoryFunction();
      } else if (type === 'constructor') {
        return new factoryFunction();
      }
    } else {
      if (type === 'factory') {
        return factoryFunction(...resolvedDependencies);
      } else if (type === 'constructor') {
        return new factoryFunction(...resolvedDependencies);
      }
    }
  });
}

export function constructor<TKeySet extends string>(factoryFunction: any): IDefinition<TKeySet> {
  return factoryOrConstructor(factoryFunction, 'constructor');
}

export function factory<TKeySet extends string>(factoryFunction: any): IDefinition<TKeySet> {
  return factoryOrConstructor(factoryFunction, 'factory');
}

export function container(): Container<never> {
  return new Container();
}

export class Container<TKeySet extends string> implements IResolver<TKeySet> {
  private definitions = new Map<string, Array<IDefinition<TKeySet>>>();

  public configure<TKey extends string>(key: TKey, definition: IDefinition<TKeySet>): Container<TKey | TKeySet> {
    const defSet = this.definitions.get(key) || [];
    this.definitions.set(key, defSet);
    defSet.push(definition);
    return this;
  }

  get resolver(): IResolver<TKeySet> {
    return this;
  }

  public resolve<T>(key: TKeySet): T {
    try {
      const definitionSet = this.definitions.get(key);
      if (!definitionSet) {
        throw new Error(`No definition exists for '${key}'.`);
      }

      const resolved = definitionSet[0].resolve(this.resolver) as T;
      return resolved;
    } catch (err) {
      throw new Error(`${err.message}\nFailed to resolve ${key}`);
    }
  }

  public resolveAll<T>(key: TKeySet): T[] {
    try {
      const definitionSet = this.definitions.get(key);
      if (!definitionSet) {
        return [];
      }

      const resolvedItems = definitionSet.map(def => def.resolve(this.resolver) as T);
      return resolvedItems;
    } catch (err) {
      throw new Error(`${err.message}\nFailed to resolve ${key}`);
    }
  }
}
