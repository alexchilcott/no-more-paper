// tslint:disable:max-classes-per-file
import { alias, constructor, container, factory, singleton, value } from './Container';

describe('container', () => {
  const TEST_VALUE = 123;
  const TEST_KEY = 'testKey';

  describe('configure value', () => {
    it('returns the registered value when resolved', () => {
      // Act
      const c = container().configure(TEST_KEY, value(TEST_VALUE));

      // Assert
      const resolved = c.resolve(TEST_KEY);
      expect(resolved).toEqual(TEST_VALUE);
    });
  });

  describe('configure alias', () => {
    it('returns the resolution of the aliased key when resolved', () => {
      // Arrange
      const aliasedKey = 'someOtherKey';

      // Act
      const c = container()
        .configure(aliasedKey, value(TEST_VALUE))
        .configure(TEST_KEY, alias(aliasedKey));

      // Assert
      const resolved = c.resolve(TEST_KEY);
      expect(resolved).toEqual(TEST_VALUE);
    });
  });

  describe('configure constructor', () => {
    it('invokes the constructor function when resolved', () => {
      class TestClass {
        public value: number;
        constructor() {
          this.value = TEST_VALUE;
        }
      }

      // Act
      const c = container().configure(TEST_KEY, constructor(TestClass));

      // Assert
      const resolved = c.resolve<any>(TEST_KEY);
      expect(resolved.value).toEqual(TEST_VALUE);
    });

    it('injects registrations with matching keys into the constructor function when resolved', () => {
      class TestClassWithDependency {
        public dependency: number;
        constructor(dependency: number) {
          this.dependency = dependency;
        }
      }

      // Act
      const c = container()
        .configure('dependency', value(TEST_VALUE))
        .configure(TEST_KEY, constructor(TestClassWithDependency));

      // Assert
      const resolved = c.resolve<any>(TEST_KEY);
      expect(resolved.dependency).toEqual(TEST_VALUE);
    });

    it('throws an error when attempting to resolve when a parameter exists that has no registration', () => {
      function TestClass(unknownDependency: number): void {
        return undefined;
      }

      // Act
      const c = container().configure(TEST_KEY, constructor(TestClass));

      // Assert
      expect(() => c.resolve<any>(TEST_KEY)).toThrowError(/unknownDependency/);
    });

    it('throws an error if not provided with a function', () => {
      expect(() => container().configure(TEST_KEY, factory('2'))).toThrowError();
    });
  });

  describe('configure factory', () => {
    it('invokes the factory function when resolved', () => {
      // Act
      const c = container().configure(TEST_KEY, factory(() => TEST_VALUE));

      // Assert
      const resolved = c.resolve<number>(TEST_KEY);
      expect(resolved).toEqual(TEST_VALUE);
    });

    it('injects registrations with matching keys into the factory function when resolved', () => {
      // Act
      const c = container()
        .configure('dependency', value(TEST_VALUE))
        .configure(TEST_KEY, factory((dependency: number) => ({ value: dependency })));

      // Assert
      const resolved = c.resolve<{ value: number }>(TEST_KEY);
      expect(resolved.value).toEqual(TEST_VALUE);
    });

    it('throws an error when attempting to resolve when a parameter exists that has no registration', () => {
      // Act
      const c = container().configure(TEST_KEY, factory((unknownDependency: any) => ({ value: unknownDependency })));

      // Assert
      expect(() => c.resolve<any>(TEST_KEY)).toThrowError(/unknownDependency/);
    });

    it('throws an error if not provided with a function', () => {
      expect(() => container().configure(TEST_KEY, factory('2'))).toThrowError();
    });
  });

  describe('configure singleton', () => {
    it('resolves the underlying dependency the first time it is resolved', () => {
      // Arrange
      let count: number = 0;
      const factoryMethod = () => {
        count = count + 1;
        return TEST_VALUE;
      };

      // Act
      const c = container().configure(TEST_KEY, singleton(factory(factoryMethod)));

      // Assert
      const resolved = c.resolve<number>(TEST_KEY);
      expect(resolved).toEqual(TEST_VALUE);
      expect(count).toEqual(1);
    });

    it('does not re-resolve the underlying dependency the second time it is resolved', () => {
      // Arrange
      let count: number = 0;
      const factoryMethod = () => {
        count = count + 1;
        return TEST_VALUE;
      };

      // Act
      const c = container().configure(TEST_KEY, singleton(factory(factoryMethod)));

      // Assert
      const resolved1 = c.resolve<number>(TEST_KEY);
      const resolved2 = c.resolve<number>(TEST_KEY);
      expect(resolved1).toEqual(TEST_VALUE);
      expect(resolved2).toEqual(TEST_VALUE);
      expect(count).toEqual(1);
    });
  });
});
