import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IEvent } from './IEvent';
import { IKafkaConsumer } from './KafkaConsumer';

export interface IKafkaEventConsumer {
  consume<T extends IEvent>(topic: string, consumerId: string): Observable<T>;
}

export class KafkaEventConsumer implements IKafkaEventConsumer {
  constructor(private readonly kafkaConsumer: IKafkaConsumer) {}

  public consume<T extends IEvent>(topic: string, consumerId: string): Observable<T> {
    return this.kafkaConsumer.consumeTopics(consumerId, topic).pipe(map(x => x.value.toString()), map(x => JSON.parse(x)));
  }
}
