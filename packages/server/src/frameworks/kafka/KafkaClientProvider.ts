import { Client } from 'kafka-node';

export interface IKafkaClientProvider {
  makeClient(): Promise<Client>;
}

export interface IKafkaClientConfiguration {
  zookeeperHost: string;
  port: number;
  clientId: string;
}

export class KafkaClientProvider implements IKafkaClientProvider {
  constructor(private readonly kafkaClientConfiguration: IKafkaClientConfiguration) {}

  public async makeClient() {
    const client = new Client(
      `${this.kafkaClientConfiguration.zookeeperHost}:${this.kafkaClientConfiguration.port}`,
      this.kafkaClientConfiguration.clientId,
      {
        sessionTimeout: 30000,
        spinDelay: 100,
        retries: 2,
      },
    );

    const onError = (err: any) => {
      console.error(err);
      process.exit(-103);
    };

    client.on('error', err => onError);
    client.on('socket_error', err => onError);
    return client;
  }
}
