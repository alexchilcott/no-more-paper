import { HighLevelProducer, ProduceRequest } from 'kafka-node';
import { IKafkaClientProvider } from './KafkaClientProvider';

export interface IKafkaProducer {
  createTopics(...topics: string[]): Promise<any>;
  publishMessages(topic: string, ...messages: any[]): Promise<any>;
}

export class KafkaProducer implements IKafkaProducer {
  private producer: Promise<HighLevelProducer>;

  constructor(private readonly kafkaClientProvider: IKafkaClientProvider) {
    this.producer = this.makeProducer();
  }

  public async createTopics(...topics: string[]): Promise<any> {
    const producer = await this.producer;
    return await new Promise((resolve, reject) => {
      producer.createTopics(topics, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  public async publishMessages(topic: string, ...messages: any[]): Promise<any> {
    const payloads: ProduceRequest[] = messages.map(message => ({
      topic,
      messages: JSON.stringify(message),
      attributes: 1 /* Use GZip compression for the payload */,
    }));
    const producer = await this.producer;
    await this.createTopics(topic);
    console.log(`Sending messages on topic ${topic}:`, messages);
    return await new Promise<any>((resolve, reject) => {
      producer.send(payloads, (error, data) => {
        if (error) {
          reject(error);
        } else {
          resolve(data);
        }
      });
    });
  }

  private makeProducer = async () => {
    const client = await this.kafkaClientProvider.makeClient();
    return new Promise<HighLevelProducer>((resolve, reject) => {
      const p = new HighLevelProducer(client);
      p.on('error', error => {
        reject(error);
      });
      p.on('ready', () => {
        resolve(p);
      });
    });
  };
}
