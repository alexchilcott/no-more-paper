import { Client, HighLevelConsumer, HighLevelConsumerOptions, Message } from 'kafka-node';
import { Observable } from 'rxjs';
import { IKafkaClientProvider } from './KafkaClientProvider';
import { IKafkaProducer } from './KafkaProducer';

export interface IKafkaConsumer {
  consumeTopics(groupId: string, ...topics: string[]): Observable<Message>;
}

export class KafkaConsumer implements IKafkaConsumer {
  constructor(private readonly kafkaProducer: IKafkaProducer, private readonly kafkaClientProvider: IKafkaClientProvider) {}

  public consumeTopics(groupId: string, ...topics: string[]): Observable<Message> {
    const topicObj = topics.map(t => ({ topic: t }));
    return new Observable<any>(subscriber => {
      const options: HighLevelConsumerOptions = {
        autoCommit: true,
        fetchMaxWaitMs: 1000,
        fetchMaxBytes: 1024 * 1024,
        encoding: 'buffer',
        groupId,
      };

      const consumerPromise = this.kafkaProducer
        .createTopics(...topics)
        .then((_: any) => this.kafkaClientProvider.makeClient())
        .then((client: Client) => {
          const consumer = new HighLevelConsumer(client, topicObj, options);
          consumer.on('message', message => subscriber.next(message));
          consumer.on('error', error => subscriber.error(error));
          consumer.on('offsetOutOfRange', error => subscriber.error(error));
          return consumer;
        })
        .catch(err => subscriber.error(err));

      return () => {
        consumerPromise.then(consumer => {
          if (consumer) {
            consumer.close(() => console.log('consumer closed'));
          }
        });
      };
    });
  }
}
