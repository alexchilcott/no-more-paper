import { IEvent } from './IEvent';
import { IKafkaProducer } from './KafkaProducer';

export interface IKafkaEventPublisher {
  publishEvents(topic: string, ...events: IEvent[]): Promise<null>;
}

export class KafkaEventPublisher implements IKafkaEventPublisher {
  constructor(private readonly kafkaProducer: IKafkaProducer) {}

  public publishEvents(topic: string, ...events: IEvent[]): Promise<null> {
    return this.kafkaProducer.publishMessages(topic, ...events);
  }
}
