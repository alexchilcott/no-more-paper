import * as del from 'del';
import * as tmp from 'tmp';
import { IDisposable } from './../utils/Disposable';

export interface ITempDirectory extends IDisposable {
  directoryPath: string;
}

export interface ITempDirectoryProvider {
  getTempDirectory(): Promise<ITempDirectory>;
}

export class TempDirectoryProvider implements ITempDirectoryProvider {
  public async getTempDirectory(): Promise<ITempDirectory> {
    const dir = await this.getDirectory();
    return {
      directoryPath: dir,
      dispose: () =>
        del(dir, { force: true }).then(_ => {
          return;
        }),
    };
  }

  private getDirectory(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      tmp.dir((err, path) => {
        if (err) {
          reject(err);
        } else {
          resolve(path);
        }
      });
    });
  }
}
