import * as fs from 'fs';

export async function copy(src: string, dest: string): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.copyFile(src, dest, err => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}
