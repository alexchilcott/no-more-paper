import { IRunnable } from './IRunnable';

export class RunnableAdapter implements IRunnable {
  constructor(readonly start: () => Promise<void>, readonly stop: () => Promise<void>) {}
}
