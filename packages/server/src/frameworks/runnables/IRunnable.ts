export interface IRunnable {
  start(): Promise<void>;
  stop(): Promise<void>;
}
