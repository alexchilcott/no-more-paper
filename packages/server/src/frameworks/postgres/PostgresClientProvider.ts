import { Client } from 'pg';

export interface IPostgresClientProvider {
  client: Promise<Client>;
}

export interface IPostgresClientProviderConfiguration {
  user: string;
  password: string;
  database: string;
  host: string;
  port: number;
}

export class PostgresClientProvider implements IPostgresClientProvider {
  public client: Promise<Client>;
  constructor(postgresClientProviderConfiguration: IPostgresClientProviderConfiguration) {
    const client = new Client({
      user: postgresClientProviderConfiguration.user,
      password: postgresClientProviderConfiguration.password,
      database: postgresClientProviderConfiguration.database,
      port: postgresClientProviderConfiguration.port,
      host: postgresClientProviderConfiguration.host,
    });

    this.client = client.connect().then(_ => client);
  }
}
