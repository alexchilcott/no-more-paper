export interface IDisposable {
  dispose(): Promise<void>;
}

export async function using<TDisposable extends IDisposable, TResult>(
  defineDisposable: () => Promise<TDisposable>,
  act: (resource: TDisposable) => Promise<TResult>,
) {
  const disposable = await defineDisposable();
  try {
    return await act(disposable);
  } finally {
    await disposable.dispose();
  }
}
