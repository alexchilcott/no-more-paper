import { exec } from 'child_process';

export async function execAsync(command: string): Promise<void> {
  await new Promise<void>((resolve, reject) => {
    exec(command, { maxBuffer: 200 * 1024 * 1024 }, (err, out, stderr) => {
      if (out) {
        console.info(out);
      }
      if (err) {
        console.error(err);
      }
      if (stderr) {
        console.error(stderr);
      }

      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}
