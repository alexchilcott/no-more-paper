import { DateTime } from 'luxon';

export interface IClock {
  utc: DateTime;
}

export class Clock implements IClock {
  get utc(): DateTime {
    return DateTime.utc();
  }
}
