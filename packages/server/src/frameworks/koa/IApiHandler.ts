import { IRouterContext } from 'koa-router';
import { IApiResponseBody, IApiResponseSendFile, IApiResponseStatusCode } from './IApiHandler';

export interface IApiResponseStatusCode<TCode extends number> {
  statusCode: TCode;
}

export interface IApiResponseSendFile {
  fileToSend: string;
  filename: string;
}

export interface IApiResponseBody<TBody> {
  body: TBody;
}

export interface IApiResponseHeaders<T> {
  headers: T;
}

type MightImplement<T> = {} | T;
type MandatoryApiResponseParts = IApiResponseStatusCode<number>;

export type IApiResponse = MightImplement<IApiResponseBody<any>> &
  MightImplement<IApiResponseHeaders<any>> &
  MightImplement<IApiResponseSendFile> &
  MandatoryApiResponseParts;

function implementsInterface<T>(response: MightImplement<T>, implementsCorrectly: (item: T) => boolean): response is T {
  const apiResponseBody = response as T;
  return implementsCorrectly(apiResponseBody);
}

export function isBodyResponse(response: IApiResponse): response is IApiResponseBody<any> & MandatoryApiResponseParts {
  return implementsInterface<IApiResponseBody<any>>(response, r => !!r.body);
}

export function isHeadersResponse(response: IApiResponse): response is IApiResponseHeaders<any> & MandatoryApiResponseParts {
  return implementsInterface<IApiResponseHeaders<any>>(response, r => !!r.headers);
}

export function isSendFileResponse(response: IApiResponse): response is IApiResponseSendFile & MandatoryApiResponseParts {
  return implementsInterface<IApiResponseSendFile>(response, r => !!r.fileToSend && !!r.filename);
}

export interface IApiRoute {
  path: string;
  method: 'HEAD' | 'OPTIONS' | 'GET' | 'PUT' | 'PATCH' | 'POST' | 'DELETE';
}

export interface IApiHandler {
  route: IApiRoute;
  handle(context: IRouterContext): Promise<IApiResponse>;
}

export interface ITypedApiHandler<TResponse extends IApiResponse> extends IApiHandler {
  handle(context: IRouterContext): Promise<TResponse>;
}
