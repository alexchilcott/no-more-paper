import { Subscription } from 'rxjs';
import { IKafkaEventConsumer } from './../kafka/KafkaEventConsumer';

export interface IConsumerManager {
  subscribe(): Promise<void>;
  unsubscribe(): Promise<void>;
}

export interface IConsumer<T> {
  topic: string;
  consumerId: string;
  handle: (event: T) => Promise<void>;
}

export class ConsumerManager implements IConsumerManager {
  public subscriptions: Subscription[] = [];
  constructor(private readonly consumers: Array<IConsumer<any>>, private readonly kafkaEventConsumer: IKafkaEventConsumer) {}

  public async subscribe(): Promise<void> {
    for (const consumer of this.consumers) {
      console.log(`Subscribing consumer with id ${consumer.consumerId} to topic ${consumer.topic}`);
      const subscription = this.kafkaEventConsumer.consume(consumer.topic, consumer.consumerId).subscribe(async event => {
        try {
          console.error(`Consumer ${consumer.consumerId} handling event:`, event.eventType);
          await consumer.handle(event);
          console.error(`Consumer ${consumer.consumerId} handled event.`, event.eventType);
        } catch (err) {
          console.error(`Consumer ${consumer.consumerId} failed to handle event:`, event.eventType, err);
        }
      });

      this.subscriptions.push(subscription);
    }
  }

  public async unsubscribe(): Promise<void> {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
}
