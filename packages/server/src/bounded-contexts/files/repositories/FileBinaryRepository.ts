import * as fs from 'fs';
import * as mkdirp from 'mkdirp';
import * as path from 'path';
import promisePipe = require('promisepipe');
import * as uuid from 'uuid';
import { stat } from '../../../frameworks/fs/stat';
import { IPostgresClientProvider } from '../../../frameworks/postgres/PostgresClientProvider';
import { IDocumentStorageConfiguration } from '../configuration/DocumentStorageConfiguration';

export interface IFileModel {
  id: string;
  filename: string;
  length: number;
}

export interface IFileRepository {
  storeFile(fileToStore: string, filename: string): Promise<string>;
  getFileById(fileId: string): Promise<IFileModel | null>;
  getLocalFilenameForId(fileId: string): string;
}

export class FileRepository implements IFileRepository {
  constructor(
    private readonly postgresClientProvider: IPostgresClientProvider,
    private readonly documentStorageConfiguration: IDocumentStorageConfiguration,
  ) {}

  public async storeFile(fileToStore: string, filename: string): Promise<string> {
    const fileId = uuid.v4();

    const fullFilePath = this.getLocalFilenameForId(fileId);
    const fullStoragePath = path.dirname(fullFilePath);
    const stats = await stat(fileToStore);
    await this.makeDirectory(fullStoragePath);
    await promisePipe(fs.createReadStream(fileToStore), fs.createWriteStream(fullFilePath));

    const client = await this.postgresClientProvider.client;
    await client.query('SELECT * FROM "InsertFile"($1::uuid, $2::text, $3::integer);', [fileId, filename, stats.size]);

    return fileId;
  }

  public async getFileById(fileId: string): Promise<IFileModel | null> {
    const client = await this.postgresClientProvider.client;
    const res = await client.query('SELECT * FROM "file" where "id" = $1::uuid', [fileId]);
    const files = res.rows.map(this.mapDocumentRowToDocumentModel);
    if (files.length !== 1) {
      return null;
    } else {
      return files[0];
    }
  }

  public getLocalFilenameForId(fileId: string): string {
    const fullStoragePath = path.resolve(this.documentStorageConfiguration.documentStoragePath);
    const fullFilePath = path.join(fullStoragePath, fileId);
    return fullFilePath;
  }

  private mapDocumentRowToDocumentModel(row: any): IFileModel {
    return {
      id: row.id,
      filename: row.filename,
      length: row.length,
    };
  }

  private makeDirectory(newPath: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      mkdirp(newPath, (err, done) => {
        if (err) {
          reject(err);
        }
        resolve();
      });
    });
  }
}
