import { join } from 'path';
import { copy } from '../../../frameworks/fs/copy';
import { ITempDirectoryProvider } from '../../../frameworks/fs/TempDirectoryProvider';
import { IDisposable } from './../../../frameworks/utils/Disposable';
import { IFileRepository } from './../repositories/FileBinaryRepository';

export interface IFile extends IDisposable {
  filename: string;
}

export interface IFileClient {
  storeFile(fileToStore: string, filename: string): Promise<string>;
  getFile(fileId: string): Promise<IFile | null>;
}

export class FileClient implements IFileClient {
  constructor(private readonly fileRepository: IFileRepository, private readonly tempDirectoryProvider: ITempDirectoryProvider) {}

  public storeFile(fileToStore: string, filename: string): Promise<string> {
    return this.fileRepository.storeFile(fileToStore, filename);
  }

  public async getFile(fileId: string): Promise<IFile | null> {
    const file = await this.fileRepository.getFileById(fileId);
    if (!file) {
      return null;
    }

    const localFilename = this.fileRepository.getLocalFilenameForId(fileId);
    const tempDir = await this.tempDirectoryProvider.getTempDirectory();
    const finalFilename = join(tempDir.directoryPath, file.filename);

    await copy(localFilename, finalFilename);

    return {
      filename: finalFilename,
      dispose: tempDir.dispose,
    };
  }
}
