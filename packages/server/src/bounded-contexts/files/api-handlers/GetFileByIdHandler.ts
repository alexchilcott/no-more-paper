import * as httpStatusCodes from 'http-status-codes';
import { IRouterContext } from 'koa-router';
import { IApiResponseStatusCode, IApiRoute, ITypedApiHandler } from '../../../frameworks/koa/IApiHandler';
import { IFileRepository } from '../repositories/FileBinaryRepository';
import { IApiResponseSendFile } from './../../../frameworks/koa/IApiHandler';

type GetFileByIdHandlerResponse = (IApiResponseStatusCode<200> & IApiResponseSendFile) | IApiResponseStatusCode<404>;

export class GetFileByIdHandler implements ITypedApiHandler<GetFileByIdHandlerResponse> {
  public route: IApiRoute = { path: '/files/:fileId/file', method: 'GET' };

  constructor(private readonly fileRepository: IFileRepository) {}

  public async handle(context: IRouterContext): Promise<GetFileByIdHandlerResponse> {
    const fileId: string = context.params.fileId;

    const file = await this.fileRepository.getFileById(fileId);
    if (file) {
      return {
        statusCode: httpStatusCodes.OK,
        fileToSend: this.fileRepository.getLocalFilenameForId(file.id),
        filename: file.filename,
      };
    } else {
      return { statusCode: httpStatusCodes.NOT_FOUND };
    }
  }
}
