import { extname } from 'path';
import { using } from '../../../frameworks/utils/Disposable';
import { IFileClient } from '../../files/client/FileClient';
import { ITextIndexResultsReadModel } from '../read-models/TextIndexResultsReadModel';
import { ITextIndexer } from '../services/TextIndexer';
import { IConsumer } from './../../../frameworks/consumers/ConsumerManager';
import { DocumentsTopicEvent, documentsTopicName } from './../../documents/events/DocumentTopicEvent';

export class TextIndexingDocumentConsumer implements IConsumer<DocumentsTopicEvent> {
  public topic: string = documentsTopicName;
  public consumerId: string = 'TextIndexingDocumentConsumer-v1.0.0';

  constructor(
    private readonly fileClient: IFileClient,
    private readonly textIndexer: ITextIndexer,
    private readonly textIndexResultsReadModel: ITextIndexResultsReadModel,
  ) {}

  public handle: (event: DocumentsTopicEvent) => Promise<void> = async (event: DocumentsTopicEvent) => {
    if (event.eventType === 'document-uploaded-event') {
      const file = await this.fileClient.getFile(event.fileId);
      if (file === null) {
        throw new Error(`File with id ${event.fileId} could not be found.`);
      }

      await using(
        async () => file,
        async f => {
          const indexResults = await this.textIndexer.indexFile(f.filename, extname(event.filename));
          if (indexResults) {
            await this.textIndexResultsReadModel.assignTextToDocument(event.documentId, indexResults.text);
          }
        },
      );
    }
  };
}
