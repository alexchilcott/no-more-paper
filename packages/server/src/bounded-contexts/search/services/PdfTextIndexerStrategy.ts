import { readFileSync } from 'fs';
import { join } from 'path';
import { using } from '../../../frameworks/utils/Disposable';
import { execAsync } from '../../../frameworks/utils/exec-async';
import { ITempDirectoryProvider } from './../../../frameworks/fs/TempDirectoryProvider';
import { ITextIndexerStrategy, ITextIndexResult } from './TextIndexer';

export class PdfTextIndexerStrategy implements ITextIndexerStrategy {
  public supportedExtensions: string[] = ['.pdf'];

  constructor(private readonly tempDirectoryProvider: ITempDirectoryProvider) {}

  public indexFile(filename: string): Promise<ITextIndexResult | null> {
    return using(
      () => this.tempDirectoryProvider.getTempDirectory(),
      async tempDir => {
        const outFile = join(tempDir.directoryPath, 'file.tiff');
        const outTextFile = join(tempDir.directoryPath, 'ocr');
        await execAsync(`convert -density 300 "${filename}" -depth 8 -strip -background white -alpha off "${outFile}"`);
        await execAsync(`tesseract -l eng "${outFile}" "${outTextFile}"`);
        const data = readFileSync(`${outTextFile}.txt`).toString();
        return { text: data };
      },
    );
  }
}
