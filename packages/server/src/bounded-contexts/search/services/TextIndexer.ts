export interface ITextIndexResult {
  text: string;
}

export interface ITextIndexer {
  indexFile(filename: string, extension: string): Promise<ITextIndexResult | null>;
}

export interface ITextIndexerStrategy {
  supportedExtensions: string[];
  indexFile(filename: string): Promise<ITextIndexResult | null>;
}

export class TextIndexer implements ITextIndexer {
  private strategies: Map<string, ITextIndexerStrategy[]> = new Map();

  constructor(textIndexingStrategies: ITextIndexerStrategy[]) {
    for (const strategy of textIndexingStrategies) {
      for (const ext of strategy.supportedExtensions) {
        const lowerExt = ext.toLowerCase();
        let strategiesForExtension = this.strategies.get(lowerExt);
        if (!strategiesForExtension) {
          strategiesForExtension = [];
          this.strategies.set(lowerExt, strategiesForExtension);
        }
        strategiesForExtension.push(strategy);
      }
    }
  }

  public async indexFile(filename: string, extension: string): Promise<ITextIndexResult | null> {
    const strategiesForExtension = this.strategies.get(extension.toLowerCase()) || [];

    for (const strategy of strategiesForExtension) {
      const result = await strategy.indexFile(filename);
      if (result) {
        return result;
      }
    }

    return null;
  }
}
