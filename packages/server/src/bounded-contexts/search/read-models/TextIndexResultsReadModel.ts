import { IPostgresClientProvider } from '../../../frameworks/postgres/PostgresClientProvider';

interface IDocumentSearchResult {
  documentId: string;
  rank: number;
}

export interface ITextIndexResultsReadModel {
  assignTextToDocument(documentId: string, text: string): Promise<void>;
  getDocumentIdsMatch(query: string): Promise<IDocumentSearchResult[]>;
}

export class TextIndexResultsReadModel implements ITextIndexResultsReadModel {
  constructor(private readonly postgresClientProvider: IPostgresClientProvider) {}

  public async assignTextToDocument(documentId: string, text: string): Promise<void> {
    const client = await this.postgresClientProvider.client;
    await client.query(
      'INSERT INTO "text_index_document_read_model"("documentId", "text") ' + 'VALUES ($1::uuid, to_tsvector(\'english\', $2::text));',
      [documentId, text],
    );
  }

  public async getDocumentIdsMatch(query: string): Promise<IDocumentSearchResult[]> {
    const client = await this.postgresClientProvider.client;
    const res = await client.query(
      'SELECT "documentId", ts_rank(text, to_tsquery($1::text)) AS "rank" FROM "text_index_document_read_model" WHERE "text" @@ to_tsquery($1::text)',
      [query],
    );
    const documentIds = res.rows.map(x => ({ documentId: x.documentId, rank: x.rank }));
    return documentIds;
  }
}
