import * as httpStatusCodes from 'http-status-codes';
import { IRouterContext } from 'koa-router';
import { ITextIndexResultsReadModel } from '../read-models/TextIndexResultsReadModel';
import { IApiResponseBody, IApiResponseStatusCode, IApiRoute, ITypedApiHandler } from './../../../frameworks/koa/IApiHandler';

interface IDocumentSearchResult {
  documentId: string;
  rank: number;
}

type DocumentSearchApiHandlerResponse =
  | IApiResponseStatusCode<400> & IApiResponseBody<string>
  | IApiResponseStatusCode<200> & IApiResponseBody<IDocumentSearchResult[]>;

export class DocumentSearchApiHandler implements ITypedApiHandler<DocumentSearchApiHandlerResponse> {
  public route: IApiRoute = { method: 'GET', path: '/search' };

  constructor(private readonly textIndexResultsReadModel: ITextIndexResultsReadModel) {}

  public async handle(context: IRouterContext): Promise<DocumentSearchApiHandlerResponse> {
    if (!this.checkQuery(context.query)) {
      return { statusCode: httpStatusCodes.BAD_REQUEST, body: 'No query specified' };
    }

    const query = context.query.q;

    const searchResults = await this.textIndexResultsReadModel.getDocumentIdsMatch(query);
    const results = searchResults.map(x => ({
      documentId: x.documentId,
      rank: x.rank,
    }));
    return { statusCode: httpStatusCodes.OK, body: results };
  }

  private checkQuery(query: any): query is { q: string } {
    if (query.q) {
      return true;
    } else {
      return false;
    }
  }
}
