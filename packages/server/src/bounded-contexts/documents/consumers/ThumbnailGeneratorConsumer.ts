import { IConsumer } from '../../../frameworks/consumers/ConsumerManager';
import { IKafkaEventPublisher } from '../../../frameworks/kafka/KafkaEventPublisher';
import { using } from '../../../frameworks/utils/Disposable';
import { DocumentsTopicEvent, documentsTopicName } from '../../documents/events/DocumentTopicEvent';
import { IFileClient } from '../../files/client/FileClient';
import { IThumbnailGenerator } from '../services/ThumbnailGenerator';
import { IDocumentThumbnailGeneratedEvent } from './../../documents/events/DocumentTopicEvent';

export class ThumbnailGeneratorConsumer implements IConsumer<DocumentsTopicEvent> {
  public topic = documentsTopicName;
  public consumerId = 'ThumbnailGeneratorConsumer-v1.0.0';

  constructor(
    private readonly fileClient: IFileClient,
    private readonly thumbnailGenerator: IThumbnailGenerator,
    private readonly kafkaEventPublisher: IKafkaEventPublisher,
  ) {}

  public handle = async (x: DocumentsTopicEvent) => {
    if (x.eventType === 'document-uploaded-event') {
      const file = await this.fileClient.getFile(x.fileId);
      if (file === null) {
        throw new Error(`File with id ${x.fileId} could not be retrieved.`);
      }

      const thumbnailInfo = await using(
        async () => file,
        async retrievedFile => {
          return await using(
            () => this.thumbnailGenerator.generateThumbnail(retrievedFile.filename),
            async generatedThumbnail => {
              const fileId = await this.fileClient.storeFile(generatedThumbnail.filename, `${x.filename}.thumb.jpg`);
              return { fileId };
            },
          );
        },
      );

      const event: IDocumentThumbnailGeneratedEvent = {
        eventType: 'document-thumbnail-generated-event',
        documentId: x.documentId,
        thumbnailFileId: thumbnailInfo.fileId,
      };
      await this.kafkaEventPublisher.publishEvents(documentsTopicName, event);
    }
  };
}
