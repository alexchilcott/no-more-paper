import { IConsumer } from '../../../frameworks/consumers/ConsumerManager';
import { assertNever } from '../../../frameworks/utils/assertNever';
import { DocumentsTopicEvent, documentsTopicName } from '../events/DocumentTopicEvent';
import { IDocumentReadModel } from './../read-models/DocumentsReadModel';
import { DateTime } from 'luxon';

export class DocumentListReadModelEventConsumer implements IConsumer<DocumentsTopicEvent> {
  public topic = documentsTopicName;
  public consumerId = 'DocumentListReadModelEventConsumer-v1.0.0';

  constructor(private readonly documentReadModel: IDocumentReadModel) {}

  public handle = async (x: DocumentsTopicEvent) => {
    switch (x.eventType) {
      case 'document-uploaded-event':
        await this.documentReadModel.addDocument(x.documentId, x.fileId, x.filename, x.size, x.uploadedTimestamp);
        break;
      case 'document-thumbnail-generated-event':
        await this.documentReadModel.setThumbnailFileId(x.documentId, x.thumbnailFileId);
        break;
      case 'document-metadata-updated-event':
        const createdTimestamp = DateTime.fromISO(x.createdTimestamp);
        await this.documentReadModel.setDocumentMetadata(x.documentId, x.title, createdTimestamp);
        break;
      default:
        assertNever(x);
    }
  };
}
