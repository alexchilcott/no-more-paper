import { DateTime } from 'luxon';
import { IPostgresClientProvider } from '../../../frameworks/postgres/PostgresClientProvider';

export interface IDocumentReadModelItem {
  documentId: string;
  fileId: string;
  uploadTime: string;
  filename: string;
  length: number;
  thumbnailFileId: string;
  title: string;
  createdTimestamp: string;
}

export interface IDocumentReadModel {
  getDocumentById(documentId: string): Promise<IDocumentReadModelItem | null>;
  getDocuments(): Promise<IDocumentReadModelItem[]>;
  addDocument(documentId: string, fileId: string, filename: string, size: number, uploadedTimestamp: string): Promise<void>;
  setThumbnailFileId(documentId: string, thumbnailFileId: string): Promise<void>;
  setDocumentMetadata(documentId: string, newTitle: string, createdTimestamp: DateTime): Promise<void>;
}

export class DocumentReadModel implements IDocumentReadModel {
  constructor(private readonly postgresClientProvider: IPostgresClientProvider) {}

  public async getDocumentById(documentId: string): Promise<IDocumentReadModelItem | null> {
    const client = await this.postgresClientProvider.client;
    const res = await client.query('SELECT * FROM "document_read_model" where "documentId" = $1::uuid', [documentId]);
    const documents = res.rows.map(this.mapDocumentRowToDocumentModel);
    if (documents.length !== 1) {
      return null;
    } else {
      return documents[0];
    }
  }

  public async getDocuments(): Promise<IDocumentReadModelItem[]> {
    const client = await this.postgresClientProvider.client;
    const res = await client.query('SELECT * FROM "document_read_model"');
    return res.rows.map(this.mapDocumentRowToDocumentModel);
  }

  public async addDocument(documentId: string, fileId: string, filename: string, size: number, uploadedTimestamp: string): Promise<void> {
    const client = await this.postgresClientProvider.client;
    await client.query(
      'INSERT INTO "document_read_model"("documentId", "fileId", "uploadTime", "filename", "length") ' +
        'VALUES ($1::uuid, $2::uuid, $3::timestamp, $4::text, $5::integer);',
      [documentId, fileId, uploadedTimestamp, filename, size],
    );
  }

  public async setThumbnailFileId(documentId: string, thumbnailFileId: string): Promise<void> {
    const client = await this.postgresClientProvider.client;
    await client.query('UPDATE "document_read_model" SET "thumbnailFileId" = $1::uuid WHERE "documentId" = $2::uuid;', [
      thumbnailFileId,
      documentId,
    ]);
  }

  public async setDocumentMetadata(documentId: string, newTitle: string, createdTimestamp: DateTime): Promise<void> {
    const client = await this.postgresClientProvider.client;
    await client.query(
      'UPDATE "document_read_model" SET "title" = $1::text, "createdTimestamp" = $2::timestamp WHERE "documentId" = $3::uuid;',
      [newTitle, createdTimestamp.toISO(), documentId]);
  }

  private mapDocumentRowToDocumentModel(row: any): IDocumentReadModelItem {
    return {
      documentId: row.documentId,
      fileId: row.fileId,
      uploadTime: DateTime.fromJSDate(row.uploadTime as Date)
        .toUTC()
        .toISO(),
      filename: row.filename,
      length: row.length,
      thumbnailFileId: row.thumbnailFileId,
      title: row.title,
      createdTimestamp: row.createdTimestamp,
    };
  }
}
