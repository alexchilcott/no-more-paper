import { join } from 'path';
import * as uuid from 'uuid';
import { execAsync } from '../../../frameworks/utils/exec-async';
import { ITempDirectoryProvider } from './../../../frameworks/fs/TempDirectoryProvider';
import { IDisposable } from './../../../frameworks/utils/Disposable';
import { IGeneratedThumbnail } from './ThumbnailGenerator';

export interface IGeneratedThumbnail extends IDisposable {
  filename: string;
}

export interface IThumbnailGenerator {
  generateThumbnail(file: string): Promise<IGeneratedThumbnail>;
}

export class ThumbnailGenerator implements IThumbnailGenerator {
  constructor(private readonly tempDirectoryProvider: ITempDirectoryProvider) {}
  public async generateThumbnail(file: string): Promise<IGeneratedThumbnail> {
    const tempDir = await this.tempDirectoryProvider.getTempDirectory();
    const fileId = uuid.v4();
    const outFile = join(tempDir.directoryPath, `${fileId}.jpg`);
    const command = `convert -background white -flatten -thumbnail x300 "${file}"[0] ${outFile}`;

    await execAsync(command);

    return {
      filename: outFile,
      dispose: tempDir.dispose,
    };
  }
}
