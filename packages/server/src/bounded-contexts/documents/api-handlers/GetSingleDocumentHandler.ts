import * as httpStatusCodes from 'http-status-codes';
import { IRouterContext } from 'koa-router';
import { IDocumentApiModel } from '../../../api-models/DocumentApiModel';
import { IApiResponseStatusCode, IApiRoute, ITypedApiHandler } from '../../../frameworks/koa/IApiHandler';
import { IDocumentReadModel } from '../read-models/DocumentsReadModel';
import { mapDocumentApiModel } from '../utils/map-document-api';
import { IApiHostConfiguration } from './../../../apihost';
import { IApiResponseBody } from './../../../frameworks/koa/IApiHandler';

type GetSingleDocumentHandlerResponse = (IApiResponseStatusCode<200> & IApiResponseBody<IDocumentApiModel>) | IApiResponseStatusCode<404>;

export class GetSingleDocumentHandler implements ITypedApiHandler<GetSingleDocumentHandlerResponse> {
  public route: IApiRoute = { path: '/documents/:documentId', method: 'GET' };

  constructor(private readonly apiHostConfiguration: IApiHostConfiguration, private readonly documentReadModel: IDocumentReadModel) {}

  public async handle(context: IRouterContext): Promise<GetSingleDocumentHandlerResponse> {
    const documentId: string = context.params.documentId;

    const document = await this.documentReadModel.getDocumentById(documentId);
    if (document) {
      const documentApiModel = mapDocumentApiModel(this.apiHostConfiguration.basePath, document);
      return { statusCode: httpStatusCodes.OK, body: documentApiModel };
    } else {
      return { statusCode: httpStatusCodes.NOT_FOUND };
    }
  }
}
