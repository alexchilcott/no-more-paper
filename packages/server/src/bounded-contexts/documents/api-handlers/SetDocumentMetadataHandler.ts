import { IRouterContext } from 'koa-router';
import { IDocumentMetadataApiModel } from '../../../api-models/DocumentMetadataApiModel';
import { IKafkaEventPublisher } from './../../../frameworks/kafka/KafkaEventPublisher';
import { IApiResponseBody, IApiResponseStatusCode, IApiRoute, ITypedApiHandler } from './../../../frameworks/koa/IApiHandler';
import { documentsTopicName, IDocumentMetadataUpdated } from './../events/DocumentTopicEvent';

type SetDocumentMetadataHandlerResponse = IApiResponseStatusCode<400> & IApiResponseBody<string> | IApiResponseStatusCode<200>;

export class SetDocumentMetadataHandler implements ITypedApiHandler<SetDocumentMetadataHandlerResponse> {
  public route: IApiRoute = { path: '/documents/:documentId/metadata', method: 'PUT' };

  constructor(private readonly kafkaEventPublisher: IKafkaEventPublisher) {}

  public async handle(context: IRouterContext): Promise<SetDocumentMetadataHandlerResponse> {
    const body = context.request.body;
    const documentId = context.params.documentId;
    if (!this.isValid(body)) {
      return { statusCode: 400, body: 'Invalid body' };
    }

    const createdTimestamp = new Date(body.createdTimestamp).toISOString();

    const event: IDocumentMetadataUpdated = {
      eventType: 'document-metadata-updated-event',
      documentId,
      title: body.title,
      createdTimestamp,
    };

    await this.kafkaEventPublisher.publishEvents(documentsTopicName, event);
    return { statusCode: 200 };
  }

  private isValid(body: IDocumentMetadataApiModel): body is IDocumentMetadataApiModel {
    if (body && body.title && body.createdTimestamp) {
      return true;
    }
    return false;
  }
}
