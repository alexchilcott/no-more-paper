import * as httpStatusCodes from 'http-status-codes';
import { IRouterContext } from 'koa-router';
import * as uuid from 'uuid';
import { IClock } from '../../../frameworks/clock/Clock';
import { IKafkaEventPublisher } from '../../../frameworks/kafka/KafkaEventPublisher';
import { IFileClient } from '../../files/client/FileClient';
import { DocumentsTopicEvent } from '../events/DocumentTopicEvent';
import { IApiHostConfiguration } from './../../../apihost';
import { IApiResponseHeaders, IApiResponseStatusCode, IApiRoute, ITypedApiHandler } from './../../../frameworks/koa/IApiHandler';

type UploadDocumentHandlerApiResponse = IApiResponseStatusCode<202> & IApiResponseHeaders<{ Location: string }>;

export class UploadDocumentHandler implements ITypedApiHandler<UploadDocumentHandlerApiResponse> {
  public route: IApiRoute = { path: '/documents', method: 'POST' };

  constructor(
    private readonly clock: IClock,
    private readonly kafkaEventPublisher: IKafkaEventPublisher,
    private readonly fileClient: IFileClient,
    private readonly apiHostConfiguration: IApiHostConfiguration,
  ) {}

  public async handle(context: IRouterContext): Promise<UploadDocumentHandlerApiResponse> {
    // Persist the file
    const uploadedFile = context.request.body.files.file;
    const uploadedFilename = uploadedFile.name;
    const uploadedFilePath = uploadedFile.path;
    const uploadedFileSize = uploadedFile.size;
    const documentId = uuid.v4();

    const fileId = await this.fileClient.storeFile(uploadedFilePath, uploadedFilename);

    const events: DocumentsTopicEvent[] = [
      {
        eventType: 'document-uploaded-event',
        fileId,
        documentId,
        filename: uploadedFilename,
        size: uploadedFileSize,
        uploadedTimestamp: this.clock.utc.toISO(),
      },
    ];

    await this.kafkaEventPublisher.publishEvents('documents', ...events);

    return {
      statusCode: httpStatusCodes.ACCEPTED,
      headers: {
        Location: `${this.apiHostConfiguration.basePath}/documents/${documentId}`,
      },
    };
  }
}
