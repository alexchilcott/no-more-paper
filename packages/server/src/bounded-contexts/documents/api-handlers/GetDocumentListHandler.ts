import * as httpStatusCodes from 'http-status-codes';
import { IRouterContext } from 'koa-router';
import { IDocumentApiModel } from '../../../api-models/DocumentApiModel';
import { IApiRoute, ITypedApiHandler } from '../../../frameworks/koa/IApiHandler';
import { IDocumentReadModel } from '../read-models/DocumentsReadModel';
import { mapDocumentApiModel } from '../utils/map-document-api';
import { IApiHostConfiguration } from './../../../apihost';
import { IApiResponseBody, IApiResponseStatusCode } from './../../../frameworks/koa/IApiHandler';

type GetDocumentListHandlerResponse = IApiResponseStatusCode<200> & IApiResponseBody<IDocumentApiModel[]>;

export class GetDocumentListHandler implements ITypedApiHandler<GetDocumentListHandlerResponse> {
  public route: IApiRoute = { path: '/documents', method: 'GET' };

  constructor(private readonly apiHostConfiguration: IApiHostConfiguration, private readonly documentReadModel: IDocumentReadModel) {}

  public async handle(context: IRouterContext): Promise<GetDocumentListHandlerResponse> {
    const documents = await this.documentReadModel.getDocuments();
    const documentApiModels = documents.map(d => mapDocumentApiModel(this.apiHostConfiguration.basePath, d));
    return {
      body: documentApiModels,
      statusCode: httpStatusCodes.OK,
    };
  }
}
