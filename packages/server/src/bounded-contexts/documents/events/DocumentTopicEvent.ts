import * as luxon from 'luxon';

export const documentsTopicName = 'documents';
export type DocumentsTopicEvent = IDocumentCreatedEvent | IDocumentThumbnailGeneratedEvent | IDocumentMetadataUpdated;

export interface IDocumentCreatedEvent {
  eventType: 'document-uploaded-event';
  documentId: string;
  fileId: string;
  filename: string;
  size: number;
  uploadedTimestamp: string;
}

export interface IDocumentThumbnailGeneratedEvent {
  eventType: 'document-thumbnail-generated-event';
  documentId: string;
  thumbnailFileId: string;
}

export interface IDocumentMetadataUpdated {
  eventType: 'document-metadata-updated-event';
  documentId: string;
  title: string;
  createdTimestamp: string;
}
