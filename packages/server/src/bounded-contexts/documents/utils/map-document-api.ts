import { IDocumentApiModel } from '../../../api-models/DocumentApiModel';
import { IDocumentReadModelItem } from '../read-models/DocumentsReadModel';

export function mapDocumentApiModel(apiBase: string, documentModel: IDocumentReadModelItem): IDocumentApiModel {
  return {
    id: documentModel.documentId,
    filename: documentModel.filename,
    size: documentModel.length,
    uploadTimestamp: documentModel.uploadTime,
    fileUrl: `${apiBase}/files/${documentModel.fileId}/file`,
    thumbnailUrl: `${apiBase}/files/${documentModel.thumbnailFileId}/file`,
    title: documentModel.title,
    createdTimestamp: documentModel.createdTimestamp,
  };
}
