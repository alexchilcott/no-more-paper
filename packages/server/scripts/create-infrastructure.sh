#!/usr/bin/env bash

docker-compose up --detach --remove-orphans
sleep 2
./deploy-databases.sh