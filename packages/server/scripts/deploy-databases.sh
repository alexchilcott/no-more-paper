#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CONTAINER_NAME=scripts_db_1
DB_USERNAME=postgres
DB_PASSWORD=postgres
DB_HOST=`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_NAME`
DB_PORT=5432

# FileStorage DB
docker exec $CONTAINER_NAME psql --username=$DB_USERNAME --command="CREATE DATABASE no_more_paper"
docker exec $CONTAINER_NAME psql --username=$DB_USERNAME --command="GRANT ALL PRIVILEGES ON DATABASE \"no_more_paper\" to $DB_USERNAME";
docker run \
    --network="container:$CONTAINER_NAME" \
    --rm \
    --volume=$SCRIPT_DIR/../database:/flyway/sql \
    boxfuse/flyway:5.0.7 \
    -url=jdbc:postgresql://$DB_HOST:$DB_PORT/no_more_paper \
    -user=$DB_USERNAME \
    -password=$DB_PASSWORD \
    migrate