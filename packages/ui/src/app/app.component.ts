import { Component } from '@angular/core';
import { ISearchBarSearchEvent } from './layout/search-bar/search-bar.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'app';
  showSearch = false;

  constructor(private readonly router: Router) {}

  onSearchChange(enabled: boolean) {
    this.showSearch = enabled;
  }

  onSearch(event: ISearchBarSearchEvent) {
    this.router.navigate(['search'], { queryParams: { q: event.query } });
  }
}
