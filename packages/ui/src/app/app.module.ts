import { MaterialModule } from './material/material.module';
import { DocumentListComponent } from './document-list/document-list.component';
import { LayoutModule } from './layout/layout.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { SearchPageComponent } from './search-page/search-page.component';
import { DocumentsPageComponent } from './documents-page/documents-page.component';
import { DocumentTableComponent } from './document-table/document-table.component';
import { EditDocumentMetadataDialogComponent } from './edit-document-metadata-dialog/edit-document-metadata-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from './store/store.module';
import { EditDocumentMetadataComponent } from './edit-document-metadata-dialog/edit-document-metadata/edit-document-metadata.component';

const routes: Route[] = [
  { path: '', redirectTo: 'documents', pathMatch: 'full' },
  { path: 'documents', component: DocumentsPageComponent },
  { path: 'search', component: SearchPageComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    SearchPageComponent,
    DocumentsPageComponent,
    DocumentListComponent,
    DocumentTableComponent,
    EditDocumentMetadataComponent,
    EditDocumentMetadataDialogComponent,
  ],
  imports: [
    StoreModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    LayoutModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [EditDocumentMetadataDialogComponent],
})
export class AppModule {}
