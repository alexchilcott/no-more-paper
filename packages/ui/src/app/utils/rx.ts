import { Observable, of, EMPTY } from 'rxjs';
import { switchMap } from 'rxjs/operators';

export function whereTruthy<T>(observableOfMaybeT: Observable<T | null | undefined>): Observable<T> {
  return observableOfMaybeT.pipe(
    switchMap(t => {
      if (t) {
        return of(t);
      } else {
        return EMPTY;
      }
    }),
  );
}

export function upcast<B, D extends B>(observable: Observable<D>): Observable<B> {
  return observable;
}
