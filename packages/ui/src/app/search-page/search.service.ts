import { map, mergeMap, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable, forkJoin, of } from 'rxjs';
import { ISearchResultApiModel, IDocumentApiModel, ApiService } from '../store/api.service';

interface IDocumentAndSearchResult {
  result: ISearchResultApiModel;
  document: IDocumentApiModel;
}

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  constructor(private readonly apiService: ApiService) {}

  public getDocumentsMatchingQuery(query: string): Observable<IDocumentApiModel[]> {
    return this.apiService
      .search(query)
      .pipe(catchError(err => []))
      .pipe(mergeMap(results => this.getDocumentsFromSearchResults(results)))
      .pipe(map(results => results.map(result => result.document)));
  }

  private getDocumentsFromSearchResults(results: ISearchResultApiModel[]): Observable<IDocumentAndSearchResult[]> {
    const observables = results.map(result => this.getDocumentFromSearchResult(result));
    if (observables.length === 0) {
      return of([]);
    }
    const o = forkJoin(observables);
    return o;
  }

  private getDocumentFromSearchResult(result: ISearchResultApiModel): Observable<IDocumentAndSearchResult> {
    return this.apiService.getDocument(result.documentId).pipe(map(doc => ({ result, document: doc })));
  }
}
