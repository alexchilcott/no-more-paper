import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { SearchService } from './search.service';
import { IDocumentApiModel } from '../store/api.service';
import { whereTruthy } from '../utils/rx';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
})
export class SearchPageComponent implements OnInit, OnDestroy {
  documents: IDocumentApiModel[];
  private subscription: Subscription;

  constructor(private readonly route: ActivatedRoute, private readonly searchService: SearchService) {}

  ngOnInit() {
    this.subscription = this.route.queryParamMap
      .pipe(map(x => x.get('q')), whereTruthy, switchMap(q => this.searchService.getDocumentsMatchingQuery(q)))
      .subscribe(results => this.searchResultsReceived(results));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private searchResultsReceived(documents: IDocumentApiModel[]) {
    this.documents = documents;
  }
}
