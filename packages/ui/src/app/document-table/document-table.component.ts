import {
  EditDocumentMetadataDialogComponent,
  IEditDocumentMetadataDialogComponentData,
} from './../edit-document-metadata-dialog/edit-document-metadata-dialog.component';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { StoreService } from '../store/store.service';
import { Observable, Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { EditMetadataDialogStateRecord } from '../edit-document-metadata-dialog/redux/EditMetadataDialogStateRecord';
import { IHideEditMetadataDialogCommand, IShowEditMetadataDialogCommand } from '../edit-document-metadata-dialog/redux/messages';
import { DocumentRecord } from '../documents-page/redux/DocumentRecord';
import { DateTime } from 'luxon';

@Component({
  selector: 'app-document-table',
  templateUrl: './document-table.component.html',
  styleUrls: ['./document-table.component.scss'],
})
export class DocumentTableComponent implements OnInit, OnDestroy {
  sub: Subscription;
  @Input() documents: DocumentRecord[];
  columnsToDisplay = ['thumbnail', 'title', 'createdTimestamp', 'actions'];

  dialogCurrentlyOpen = false;
  dialogState$: Observable<EditMetadataDialogStateRecord>;

  constructor(public readonly dialog: MatDialog, public readonly store: StoreService) { }

  ngOnInit(): void {
    this.dialogState$ = this.store.select(x => x.editMetadataDialogState);

    this.sub = this.dialogState$.subscribe(state => {
      const shouldBeOpen = state.get('isOpen');
      if (shouldBeOpen !== this.dialogCurrentlyOpen) {
        if (shouldBeOpen) {
          const document = state.get('document');
          const dialog = this.dialog.open(EditDocumentMetadataDialogComponent, {
            width: '600px',
            data: { document } as IEditDocumentMetadataDialogComponentData,
          });

          dialog.disableClose = true;
          const dialogCloseSubscription = dialog
            .keydownEvents()
            .pipe(filter(x => x.keyCode === 27))
            .subscribe(_ => {
              const action: IHideEditMetadataDialogCommand = { type: 'hide-edit-metadata-dialog' };
              this.store.dispatch(action);
            });
          dialog
            .beforeClose()
            .pipe(first())
            .subscribe(_ => {
              dialogCloseSubscription.unsubscribe();
            });
        } else {
          this.dialog.closeAll();
        }
        this.dialogCurrentlyOpen = shouldBeOpen;
      }
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  openEditDialog(document: DocumentRecord): void {
    const action: IShowEditMetadataDialogCommand = { type: 'show-edit-metadata-dialog', document: document };
    this.store.dispatch(action);
  }

  formatTimestamp(timestamp: string) {
    const time = DateTime.fromISO(timestamp).setZone('utc').setZone('local', { keepLocalTime: true }).toJSDate();
    return time.toLocaleDateString() + ' ' + time.toLocaleTimeString();
  }
}
