import { Component, Input } from '@angular/core';
import { List } from 'immutable';
import { DocumentRecord } from '../documents-page/redux/DocumentRecord';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss'],
})
export class DocumentListComponent {
  @Input() documents: List<DocumentRecord>;
}
