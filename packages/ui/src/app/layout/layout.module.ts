import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module';
import { UploadComponent } from './navbar/upload/upload.component';
import { FileUploadService } from './navbar/upload/file-upload.service';
import { SearchBarComponent } from './search-bar/search-bar.component';

@NgModule({
  imports: [CommonModule, RouterModule, MaterialModule],
  declarations: [NavbarComponent, UploadComponent, SearchBarComponent],
  exports: [NavbarComponent, SearchBarComponent],
  providers: [FileUploadService],
})
export class LayoutModule {}
