import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  @Input() title: string;
  @Input() searchSelected = false;
  @Output() searchChange: EventEmitter<boolean> = new EventEmitter();

  searchToggled() {
    this.searchSelected = !this.searchSelected;
    this.searchChange.next(this.searchSelected);
  }
}
