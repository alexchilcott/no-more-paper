import { Component, OnInit } from '@angular/core';
import { FileUploadService } from './file-upload.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
})
export class UploadComponent implements OnInit {
  constructor(private readonly fileUploadService: FileUploadService) {}

  ngOnInit() {}

  selectEvent(event) {
    const files: File[] = event.srcElement.files;
    if (!files) {
      return;
    }

    this.fileUploadService.uploadFile(files[0]);
  }
}
