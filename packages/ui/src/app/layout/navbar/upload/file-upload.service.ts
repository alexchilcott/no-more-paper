import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FileUploadService {
  constructor(private readonly httpClient: HttpClient) {}

  uploadFile(file: File) {
    const path = `/api/documents`;
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    this.httpClient.post(path, formData).subscribe(r => {
      console.log('got r', r);
    });
  }
}
