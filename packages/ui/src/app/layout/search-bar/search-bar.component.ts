import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Subject, interval, Subscription } from 'rxjs';
import { debounce } from 'rxjs/operators';

export interface ISearchBarSearchEvent {
  query: string;
}

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit, OnDestroy {
  @Output() search: EventEmitter<ISearchBarSearchEvent> = new EventEmitter();

  private searchSubject: Subject<string> = new Subject<string>();
  private subscription?: Subscription;

  ngOnInit() {
    this.subscription = this.searchSubject.pipe(debounce(_ => interval(400))).subscribe(x => this.search.next({ query: x }));
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onSearchQueryChange(value: string) {
    this.searchSubject.next(value);
  }
}
