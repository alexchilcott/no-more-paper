import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatButtonToggleModule,
  MatTableModule,
  MatDialogModule,
  MatDatepickerModule,
  MatNativeDateModule,
} from '@angular/material';
import { NgModule } from '@angular/core';

const modules = [
  MatDatepickerModule,
  MatNativeDateModule,
  MatDialogModule, MatTableModule, MatButtonModule, MatCardModule, MatIconModule, MatInputModule, MatButtonToggleModule];

@NgModule({
  imports: modules,
  exports: modules,
})
export class MaterialModule {}
