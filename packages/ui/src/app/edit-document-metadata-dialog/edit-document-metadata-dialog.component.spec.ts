import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDocumentMetadataDialogComponent } from './edit-document-metadata-dialog.component';

describe('EditDocumentMetadataDialogComponent', () => {
  let component: EditDocumentMetadataDialogComponent;
  let fixture: ComponentFixture<EditDocumentMetadataDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditDocumentMetadataDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDocumentMetadataDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
