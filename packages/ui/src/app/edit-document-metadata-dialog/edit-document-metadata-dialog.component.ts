import { StoreService } from './../store/store.service';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IDocumentMetadata } from './edit-document-metadata/edit-document-metadata.component';
import { DocumentRecord } from '../documents-page/redux/DocumentRecord';

export interface IEditDocumentMetadataDialogComponentData {
  document: DocumentRecord;
}

@Component({
  selector: 'app-edit-document-metadata-dialog',
  templateUrl: './edit-document-metadata-dialog.component.html',
  styleUrls: ['./edit-document-metadata-dialog.component.scss'],
})
export class EditDocumentMetadataDialogComponent {
  documentMetadata: IDocumentMetadata;
  constructor(
    private readonly dialogRef: MatDialogRef<EditDocumentMetadataDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public readonly data: IEditDocumentMetadataDialogComponentData,
    private readonly store: StoreService,
  ) {
    this.documentMetadata = {
      title: data.document.get('title'),
      createdTimestamp: data.document.get('createdTimestamp').toISO(),
    };
  }

  onCancel() {
    this.dialogRef.close();
  }

  documentMetadataChanged(documentMetadata: IDocumentMetadata) {
    this.documentMetadata = documentMetadata;
  }

  onSubmit() {
    this.store.dispatch({
      type: 'edit-document-metadata',
      documentId: this.data.document.get('id'),
      metadata: this.documentMetadata,
    });
  }
}
