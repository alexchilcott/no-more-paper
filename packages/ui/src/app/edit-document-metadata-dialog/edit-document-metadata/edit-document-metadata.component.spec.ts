import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDocumentMetadataComponent } from './edit-document-metadata.component';

describe('EditDocumentMetadataComponent', () => {
  let component: EditDocumentMetadataComponent;
  let fixture: ComponentFixture<EditDocumentMetadataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditDocumentMetadataComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDocumentMetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
