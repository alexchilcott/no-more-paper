import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { DateTime } from 'luxon';
import { map } from 'rxjs/operators';

export interface IDocumentMetadata {
  title: string;
  createdTimestamp: string;
}

interface IDocumentMetadataFormData { title: string; createdTimestamp: Date; }
type DocumentMetadataForm = { [P in keyof IDocumentMetadataFormData]: AbstractControl };

@Component({
  selector: 'app-edit-document-metadata',
  templateUrl: './edit-document-metadata.component.html',
  styleUrls: ['./edit-document-metadata.component.scss'],
})
export class EditDocumentMetadataComponent implements OnChanges, OnInit {
  @Input() documentMetadata: IDocumentMetadata;
  @Output() documentMetadataChanged: EventEmitter<IDocumentMetadata> = new EventEmitter();

  form = new FormGroup({
    title: new FormControl(),
    createdTimestamp: new FormControl(),
  } as DocumentMetadataForm);

  ngOnInit(): void {
    this.form.valueChanges
    .pipe(
      map(x => ({title: x.title, createdTimestamp: x.createdTimestamp} as IDocumentMetadataFormData)),
      map(x => {
        const date = DateTime.fromJSDate(x.createdTimestamp);
        const dateNoTimezone = date.setZone('utc', {keepLocalTime: true}).toISO();
        return {
          ...x,
          createdTimestamp: dateNoTimezone,
        } as IDocumentMetadata;
      })
    )
    .subscribe(v => this.documentMetadataChanged.next(v));
  }

  ngOnChanges(changes: SimpleChanges): void {
    const documentPropertyName: keyof EditDocumentMetadataComponent = 'documentMetadata';
    if (changes[documentPropertyName]) {
      const documentMetadata: IDocumentMetadata = changes[documentPropertyName].currentValue;
      const date = DateTime.fromISO(documentMetadata.createdTimestamp).setZone('utc').setZone('local', {keepLocalTime: true}).toJSDate();
      const documentMetadataStrict: IDocumentMetadataFormData = {
        title: documentMetadata.title,
        createdTimestamp: date,
      };
      this.form.setValue(documentMetadataStrict);
    }
  }
}
