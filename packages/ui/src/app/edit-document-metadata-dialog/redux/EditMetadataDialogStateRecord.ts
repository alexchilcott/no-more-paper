import { Record } from 'immutable';
import { DocumentRecord } from '../../documents-page/redux/DocumentRecord';

interface ITypedObject {
  isOpen: boolean;
  document: DocumentRecord | undefined;
}

const defaultData: ITypedObject = {
  isOpen: false,
  document: undefined,
};

export class EditMetadataDialogStateRecord extends Record(defaultData) {
  constructor(params: ITypedObject) {
    super(params);
  }
  get<T extends keyof ITypedObject>(key: T): ITypedObject[T] {
    return super.get(key);
  }
  set<T extends keyof ITypedObject>(key: T, value: ITypedObject[T]): EditMetadataDialogStateRecord {
    return super.set(key, value);
  }
}
