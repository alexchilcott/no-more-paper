import { IDocumentMetadata } from '../edit-document-metadata/edit-document-metadata.component';
import { DocumentRecord } from '../../documents-page/redux/DocumentRecord';

export interface IShowEditMetadataDialogCommand {
  type: 'show-edit-metadata-dialog';
  document: DocumentRecord;
}

export interface IHideEditMetadataDialogCommand {
  type: 'hide-edit-metadata-dialog';
}

export interface IEditDocumentMetadataCommand {
  type: 'edit-document-metadata';
  documentId: string;
  metadata: IDocumentMetadata;
}

export interface IEditDocumentMetadataStartedEvent {
  type: 'edit-document-metadata-started';
  documentId: string;
  metadata: IDocumentMetadata;
}

export interface IEditDocumentMetadataFinishedEvent {
  type: 'edit-document-metadata-finished';
  documentId: string;
  metadata: IDocumentMetadata;
}

export type EditMetadataEvent = IEditDocumentMetadataStartedEvent | IEditDocumentMetadataFinishedEvent;

export type EditMetadataCommand = IEditDocumentMetadataCommand | IShowEditMetadataDialogCommand | IHideEditMetadataDialogCommand;
