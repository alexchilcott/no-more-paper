import { Epic } from 'redux-observable';
import { IEditDocumentMetadataCommand, IEditDocumentMetadataStartedEvent, IEditDocumentMetadataFinishedEvent } from './messages';
import { map, startWith } from 'rxjs/operators';
import { ApiService, IDocumentMetadataApiModel } from '../../store/api.service';
import { Message } from '../../store/store/messages';
import { IAppState } from '../../store/store/state';
import { when } from '../../store/store/epicUtils';

const editDocumentMetadataActions = (apiService: ApiService) => (action: IEditDocumentMetadataCommand) => {
  const startedAction: IEditDocumentMetadataStartedEvent = {
    type: 'edit-document-metadata-started',
    documentId: action.documentId,
    metadata: action.metadata,
  };

  const finishedAction: IEditDocumentMetadataFinishedEvent = {
    type: 'edit-document-metadata-finished',
    documentId: action.documentId,
    metadata: action.metadata,
  };

  const metadata: IDocumentMetadataApiModel = {
    title: action.metadata.title,
    createdTimestamp: action.metadata.createdTimestamp,
  };

  return apiService
    .putDocumentMetadata(action.documentId, metadata)
    .pipe(map(_ => finishedAction), startWith<Message>(startedAction));
};

export function* createEditMetadataEpics(apiService: ApiService): IterableIterator<Epic<Message, Message, IAppState>> {
  yield when<IEditDocumentMetadataCommand>('edit-document-metadata').returnStream(editDocumentMetadataActions(apiService));

  yield when<IEditDocumentMetadataFinishedEvent>('edit-document-metadata-finished').fire(m => ({
    type: 'refresh-document',
    documentId: m.documentId,
  }));

  yield when('edit-document-metadata-finished').fire({ type: 'hide-edit-metadata-dialog' });
}
