import { EditMetadataDialogStateRecord } from './EditMetadataDialogStateRecord';
import { EDIT_METADATA_DIALOG_INITIAL_STATE } from './initial-state';
import { Reducer } from 'redux';
import { Message } from '../../store/store/messages';

export const editDocumentMetadataReducer: Reducer<EditMetadataDialogStateRecord, Message> = (state, action) => {
  if (state === undefined) {
    return EDIT_METADATA_DIALOG_INITIAL_STATE;
  }
  switch (action.type) {
    case 'show-edit-metadata-dialog':
      state = state.set('isOpen', true).set('document', action.document);
      break;
    case 'hide-edit-metadata-dialog':
      state = state.set('isOpen', false).set('document', undefined);
      break;
  }
  return state;
};
