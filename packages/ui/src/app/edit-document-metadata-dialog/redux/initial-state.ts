import { EditMetadataDialogStateRecord } from './EditMetadataDialogStateRecord';

export const EDIT_METADATA_DIALOG_INITIAL_STATE: EditMetadataDialogStateRecord = new EditMetadataDialogStateRecord({
  isOpen: false,
  document: undefined,
});
