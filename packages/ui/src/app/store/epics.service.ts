import { Injectable } from '@angular/core';
import { Epic } from 'redux-observable';
import { ApiService } from './api.service';
import { IAppState } from './store/state';
import { Message } from './store/messages';
import { createEditMetadataEpics } from '../edit-document-metadata-dialog/redux/epics';
import { createDocumentsEpics } from '../documents-page/redux/epics';

function* flatten<T>(items: Iterable<Iterable<T>>): Iterable<T> {
  for (const outer of items) {
    for (const inner of outer) {
      yield inner;
    }
  }
}

@Injectable({ providedIn: 'root' })
export class EpicsService {
  constructor(private apiService: ApiService) {}

  public createEpics(): Epic<Message, Message, IAppState>[] {
    const iterators: Iterable<Iterable<Epic<Message, Message, IAppState>>> = [
      createEditMetadataEpics(this.apiService),
      createDocumentsEpics(this.apiService),
    ];

    const flattened = Array.from(flatten(iterators));
    return flattened;
  }
}
