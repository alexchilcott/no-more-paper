import { Message } from './messages';
import { ActionsObservable } from 'redux-observable';
import { map, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

class WhenEpicGenerator<T extends Message> {
  constructor(private readonly key: T['type']) {}

  fire(response: T | ((message: T) => Message)) {
    if (typeof response === 'function') {
      return this.returnStream(m => of(response(m)));
    } else {
      return this.fire(_ => response);
    }
  }

  returnStream(getActions: ((action: T) => Observable<Message>)) {
    return (action$: ActionsObservable<Message>) => action$.ofType(this.key).pipe(map(a => a as T), switchMap(getActions));
  }
}

export const when = <T extends Message>(key: T['type']) => new WhenEpicGenerator<T>(key);
