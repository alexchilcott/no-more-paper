import { combineReducers } from 'redux';
import { IAppState } from './state';
import { Message } from './messages';
import { editDocumentMetadataReducer } from '../../edit-document-metadata-dialog/redux/reducer';
import { documentReducer } from '../../documents-page/redux/reducer';

export const rootReducer: (state: IAppState | undefined, action: Message) => IAppState = combineReducers({
  documents: documentReducer,
  editMetadataDialogState: editDocumentMetadataReducer,
});
