import { List } from 'immutable';
import { EditMetadataDialogStateRecord } from '../../edit-document-metadata-dialog/redux/EditMetadataDialogStateRecord';
import { DocumentRecord } from '../../documents-page/redux/DocumentRecord';

export interface IAppState {
  documents: List<DocumentRecord>;
  editMetadataDialogState: EditMetadataDialogStateRecord;
}
