import { EditMetadataEvent, EditMetadataCommand } from '../../edit-document-metadata-dialog/redux/messages';
import { DocumentsEvent, DocumentsCommand } from '../../documents-page/redux/messages';

export type EventMessage = DocumentsEvent | EditMetadataEvent;
export type CommandMessage = DocumentsCommand | EditMetadataCommand;
export type Message = EventMessage | CommandMessage;
