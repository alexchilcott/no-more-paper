import { TestBed, inject } from '@angular/core/testing';

import { EpicsService } from './epics.service';

describe('EpicsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EpicsService],
    });
  });

  it(
    'should be created',
    inject([EpicsService], (service: EpicsService) => {
      expect(service).toBeTruthy();
    }),
  );
});
