import { createEpicMiddleware, combineEpics } from 'redux-observable';
import { ApiService } from './api.service';
import { EpicsService } from './epics.service';
import { NgModule, isDevMode } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgReduxModule, NgRedux, DevToolsExtension } from '@angular-redux/store';
import { createStore, applyMiddleware, compose, StoreEnhancer } from 'redux';
import { StoreService } from './store.service';
import { IAppState } from './store/state';
import { Message } from './store/messages';
import { rootReducer } from './store/reducer';

@NgModule({
  imports: [CommonModule, NgReduxModule],
  declarations: [],
  providers: [EpicsService, ApiService, StoreService],
})
export class StoreModule {
  constructor(ngRedux: NgRedux<IAppState>, devTools: DevToolsExtension, epicsService: EpicsService) {
    const epics = epicsService.createEpics();
    const rootEpic = combineEpics(...epics);
    const epicMiddleware = createEpicMiddleware<Message, Message, IAppState>();

    let enhancers: StoreEnhancer[] = [applyMiddleware(epicMiddleware)];

    if (isDevMode() && devTools.isEnabled()) {
      enhancers = [...enhancers, devTools.enhancer()];
    }

    const store = createStore(rootReducer, compose(...enhancers));
    epicMiddleware.run(rootEpic);
    ngRedux.provideStore(store);
  }
}
