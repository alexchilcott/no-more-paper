import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { Observable } from 'rxjs';
import { CommandMessage } from './store/messages';
import { IAppState } from './store/state';

@Injectable({
  providedIn: 'root',
})
export class StoreService {
  constructor(private readonly redux: NgRedux<IAppState>) {}

  dispatch(t: CommandMessage) {
    this.redux.dispatch(t);
  }

  select<T>(selector: ((appState: IAppState) => T)): Observable<T> {
    return this.redux.select(x => selector(x));
  }
}
