import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface IDocumentApiModel {
  id: string;
  filename: string;
  size: number;
  uploadTimestamp: string;
  fileUrl: string;
  thumbnailUrl: string;
  title: string;
  createdTimestamp: string;
}

export interface IDocumentMetadataApiModel {
  title: string;
  createdTimestamp: string;
}

export interface ISearchResultApiModel {
  documentId: string;
  rank: number;
}

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private readonly httpClient: HttpClient) {}

  getDocuments(): Observable<IDocumentApiModel[]> {
    return this.httpClient.get<IDocumentApiModel[]>('/api/documents');
  }

  getDocument(id: string): Observable<IDocumentApiModel> {
    return this.httpClient.get<IDocumentApiModel>(`/api/documents/${id}`);
  }

  putDocumentMetadata(documentId: string, metadata: IDocumentMetadataApiModel): Observable<{}> {
    return this.httpClient.put<IDocumentMetadataApiModel>(`/api/documents/${documentId}/metadata`, metadata).pipe(map(x => ({})));
  }

  search(query: string): Observable<ISearchResultApiModel[]> {
    return this.httpClient.get<ISearchResultApiModel[]>(`/api/search?q=${query}`);
  }
}
