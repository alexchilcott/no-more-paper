import { Record } from 'immutable';
import { DateTime } from 'luxon';

interface ITypedObject {
  id: string;
  filename: string;
  fileSize: number;
  uploadTimestamp: string;
  fileUrl: string;
  thumbnailUrl: string;
  title: string;
  createdTimestamp: DateTime;
}

const defaultData: ITypedObject = {
  id: '',
  filename: '',
  fileSize: 0,
  uploadTimestamp: '',
  fileUrl: '',
  thumbnailUrl: '',
  title: '',
  createdTimestamp: DateTime.utc(2001, 1, 1),
};

export class DocumentRecord extends Record(defaultData) {
  constructor(params: ITypedObject) {
    super(params);
  }
  get<T extends keyof ITypedObject>(value: T): ITypedObject[T] {
    return super.get(value);
  }
  set<T extends keyof ITypedObject>(key: T, value: ITypedObject[T]): DocumentRecord {
    return super.set(key, value);
  }
}
