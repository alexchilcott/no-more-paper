import { DocumentRecord } from './DocumentRecord';
import { List } from 'immutable';

export const DOCUMENTS_INITIAL_STATE: List<DocumentRecord> = List<DocumentRecord>();
