import { DocumentRecord } from './DocumentRecord';

export interface IReloadAllDocumentsCommand {
  type: 'reload-all-documents';
}

export interface IRefreshDocumentCommand {
  type: 'refresh-document';
  documentId: string;
}

export interface IDocumentLoadedEvent {
  type: 'document-loaded';
  document: DocumentRecord;
}

export interface IReloadAllDocumentsStartedEvent {
  type: 'reload-all-documents-started';
}

export interface IReloadAllDocumentsFinishedEvent {
  type: 'reload-documents-finished';
  documents: DocumentRecord[];
}

export type DocumentsCommand = IReloadAllDocumentsCommand | IRefreshDocumentCommand;
export type DocumentsEvent = IReloadAllDocumentsStartedEvent | IReloadAllDocumentsFinishedEvent | IDocumentLoadedEvent;
