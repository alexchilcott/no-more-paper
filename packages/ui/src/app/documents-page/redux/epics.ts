import { Epic } from 'redux-observable';
import { DocumentsEvent, IReloadAllDocumentsCommand, IRefreshDocumentCommand, IDocumentLoadedEvent } from './messages';
import { map, startWith } from 'rxjs/operators';
import { DocumentRecord } from './DocumentRecord';
import { IDocumentApiModel, ApiService } from '../../store/api.service';
import { Message } from '../../store/store/messages';
import { IAppState } from '../../store/store/state';
import { when } from '../../store/store/epicUtils';
import { DateTime } from 'luxon';

const mapDocumentModel: (document: IDocumentApiModel) => DocumentRecord = x =>
  new DocumentRecord({
    id: x.id,
    filename: x.filename,
    fileSize: x.size,
    uploadTimestamp: x.uploadTimestamp,
    fileUrl: x.fileUrl,
    thumbnailUrl: x.thumbnailUrl,
    title: x.title,
    createdTimestamp: DateTime.fromISO(x.createdTimestamp),
  });

const mapDocumentModels = (documents: IDocumentApiModel[]) => documents.map(mapDocumentModel);

function reloadAllDocumentsActions(apiService: ApiService) {
  const startedEvent: DocumentsEvent = { type: 'reload-all-documents-started' };
  const documentsReloadedAction: (documents: IDocumentApiModel[]) => DocumentsEvent = documents => ({
    type: 'reload-documents-finished',
    documents: mapDocumentModels(documents),
  });

  return apiService.getDocuments().pipe(map(documents => documentsReloadedAction(documents)), startWith<DocumentsEvent>(startedEvent));
}

function reloadDocumentAction(apiService: ApiService, documentId: string) {
  const documentReloadedAction = (document: IDocumentApiModel) => {
    return {
      type: 'document-loaded',
      document: mapDocumentModel(document),
    } as IDocumentLoadedEvent;
  };
  return apiService.getDocument(documentId).pipe(map(document => documentReloadedAction(document)));
}

export function* createDocumentsEpics(apiService: ApiService): Iterable<Epic<Message, Message, IAppState>> {
  yield when<IRefreshDocumentCommand>('refresh-document').returnStream(e => reloadDocumentAction(apiService, e.documentId));
  yield when<IReloadAllDocumentsCommand>('reload-all-documents').returnStream(_ => reloadAllDocumentsActions(apiService));
}
