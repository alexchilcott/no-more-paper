import { DOCUMENTS_INITIAL_STATE } from './initial-state';
import { Reducer } from 'redux';
import { DocumentRecord } from './DocumentRecord';
import { List } from 'immutable';
import * as _ from 'lodash';
import { Message } from '../../store/store/messages';

export const documentReducer: Reducer<List<DocumentRecord>, Message> = (state, action) => {
  if (state === undefined) {
    return DOCUMENTS_INITIAL_STATE;
  }
  switch (action.type) {
    case 'reload-all-documents-started':
      return state;
    case 'reload-documents-finished':
      return List<DocumentRecord>(_(action.documents).sortBy(['id']));
    case 'document-loaded':
      const newList = state
        .filter(x => !!x && x.get('id') !== action.document.get('id'))
        .toArray()
        .concat(action.document);
      const sorted = _(newList).sortBy(['id']);
      return List<DocumentRecord>(sorted);
  }
  return state;
};
