import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { StoreService } from '../store/store.service';
import { CommandMessage } from '../store/store/messages';
import { DocumentRecord } from './redux/DocumentRecord';

@Component({
  selector: 'app-documents-page',
  templateUrl: './documents-page.component.html',
  styleUrls: ['./documents-page.component.scss'],
})
export class DocumentsPageComponent implements OnInit {
  documents$: Observable<DocumentRecord[]>;

  constructor(private store: StoreService) {}

  ngOnInit() {
    const action: CommandMessage = { type: 'reload-all-documents' };
    this.store.dispatch(action);
    this.documents$ = this.store.select(x => {
      return x.documents.toArray();
    });
  }
}
