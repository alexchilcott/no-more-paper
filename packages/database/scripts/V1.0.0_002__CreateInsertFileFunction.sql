CREATE FUNCTION "InsertFile"("_id" uuid, "_filename" text, "_length" integer)
RETURNS void AS
  $BODY$
      BEGIN
        INSERT INTO "file"("id", "filename", "length")
        VALUES("_id", "_filename", "_length");
      END;
  $BODY$
  LANGUAGE 'plpgsql';
