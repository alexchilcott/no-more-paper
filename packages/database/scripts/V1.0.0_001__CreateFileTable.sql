CREATE TABLE "file" (
  "id" uuid NOT NULL,
  "filename" text NOT NULL,
  "length" integer NOT NULL
);

ALTER TABLE "file" ADD CONSTRAINT "file_id" PRIMARY KEY ("id");