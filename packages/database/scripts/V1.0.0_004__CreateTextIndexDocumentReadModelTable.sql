CREATE TABLE "text_index_document_read_model" (
  "documentId" uuid NOT NULL,
  "text" tsvector NOT NULL
);

ALTER TABLE "text_index_document_read_model" ADD CONSTRAINT "text_index_document_read_model_id" PRIMARY KEY ("documentId");