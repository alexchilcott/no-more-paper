CREATE TABLE "document_read_model" (
  "documentId" uuid NOT NULL,
  "fileId" uuid NOT NULL,
  "uploadTime" timestamp with time zone NOT NULL,
  "filename" text NOT NULL,
  "length" integer NOT NULL,
  "thumbnailFileId" uuid NULL
);

ALTER TABLE "document_read_model" ADD CONSTRAINT "document_read_model_id" PRIMARY KEY ("documentId");