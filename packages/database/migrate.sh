#!/usr/bin/env bash

DB_HOST=db
DB_PORT=5432
DB_USERNAME=postgres
DB_PASSWORD=postgres

flyway \
    -url=jdbc:postgresql://$DB_HOST:$DB_PORT/no_more_paper \
    -user=$DB_USERNAME \
    -password=$DB_PASSWORD \
    migrate