#!/bin/bash
sleep 10
envsubst '${NMP_API_HOST} ${NMP_UI_HOST}' </etc/nginx/nginx.conf.template >/etc/nginx/nginx.conf
echo "Running nginx with config:"
cat /etc/nginx/nginx.conf
nginx -g 'daemon off;'